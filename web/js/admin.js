function checkDeleteTeam(){
    return confirm("Are you sure you want to delete this Team?");
}

function checkRemoveMember(){
    return confirm("Are you sure you want to remove this member?");
}

function checkDeleteRole(){
    return confirm("Are you sure you want to delete this Role?");
}

function checkDeleteImage(){
    return confirm("Are you sure you want to delete this Image?");
}