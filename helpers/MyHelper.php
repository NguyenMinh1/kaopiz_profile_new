<?php 
	namespace app\helpers;
	use Yii;
	use yii\data\Pagination;
	use app\models\Role;
	class MyHelper{
		public static function getBaseUrl(){
			return '/kaopiz-profile/';
		}

		public static function isMe($idProfile){
			return Yii::$app->session['id'] == $idProfile;
		}

		public static function isAdmin(){
			$role = Yii::$app->authManager->getRole('Admin');
			$rolesById = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
			return in_array($role, $rolesById);
		}

		public static function getAge($birthday){
			if($birthday == null) return 0;
			else {
				$arr = explode('-', $birthday);
				$year = intval($arr[0]);
				$current_year = intval(date("Y"));
				return $current_year - $year;
			}
		}

		public static function getPagination($page_size, $total_count){
			$pagination = new Pagination([
				'defaultPageSize' => $page_size,
				'totalCount' => $total_count,
			]);
			return $pagination;
		}

		public static function getYear($birthday){
			if($birthday == null) return '';
			else {
				$year = explode('-', $birthday)[0];
				return $year;
			}
		}

		public static function getCurrentUrl(){
			return 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		}

		public static function getPrevUrl(){
			return $_SERVER['HTTP_REFERER'];
		}
 	}
 ?>