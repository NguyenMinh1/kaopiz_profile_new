<?php

namespace app\controllers;

use yii\web\Controller;
use Yii;
use app\models\Team;
use app\models\Image;
use yii\helpers\Url;
use app\models\JoinTeam;

Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
    'css' => [],
    'js' => []
];


class TeamController extends Controller
{
    public $img_slides = '';
    public $layout = 'layout';

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest)
            $this->redirect(Url::to(['/member/login']));
        $this->img_slides = Image::find()->asArray()->where(['member_id' => '-1'])->all();
        return parent::beforeAction($action);
    }

    public function actionTeam($team_id = -1)
    {
        $model = Team::find()->asArray()->orderBy('team.name')->all();
        if ($model && $team_id == -1) {
            $team_id = $model[0]['id'];
        }
        $jointeam = JoinTeam::findOne(['member_id' => Yii::$app->user->id, 'team_id' => $team_id]);
        if ($jointeam != null)
            $join = 0;
        else $join = 1;
        $leaveteam = JoinTeam::findOne(['member_id' => Yii::$app->user->id, 'team_id' => $team_id, 'active' => '1']);
        if ($leaveteam == null)
            $leave = 0;
        else $leave = 1;
        $members = Yii::$app->db->createCommand('select team.id as team_id,team.name as team_name,team.desc,member.name,member.birthday,member.sex,member.phone,member.avatar,member.id,join_team.active from team left join join_team on team.id=join_team.team_id left join member on join_team.member_id=member.id where  team.id=:team_id order by member.birthday', [':team_id' => $team_id])->queryAll();
        return $this->render('list_team', ['list_team' => $model, 'members' => $members, 'img_slides' => $this->img_slides,'leave'=>$leave,'join'=>$join]);
    }


}
