<?php

namespace app\controllers;

use yii\web\Controller;
use Yii;
use app\models\Member;
use app\models\Image;

Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
    'css' => [],
    'js' => []
];

class ProfileController extends Controller
{
    public $img_slides = '';
    public $layout = 'layout';

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest)
            $this->redirect(['/member/login']);
        $this->img_slides = Image::find()->asArray()->where(['member_id' => '-1'])->all();
        return parent::beforeAction($action);
    }

    public function actionMyProfile()
    {
        $model = Member::findById(Yii::$app->user->id);
        $list_img = Image::find()->select('image')->asArray()->where(['member_id' => Yii::$app->user->id])->all();
        if ($model->load(Yii::$app->request->post(), "Member")) {
            $model->save(false);
        }
        return $this->render('my_profile', ['model' => $model, 'list_img' => $list_img, 'img_slides' => $this->img_slides]);
    }

    public function actionMemberProfile($member_id)
    {
        $model = Member::findById($member_id);
        $list_img = Image::find()->select('image')->asArray()->where(['member_id' => $member_id])->all();
        if (Yii::$app->user->can('editOwnProfile', ['member' => $model]))
            $this->redirect(['profile/my-profile']);
        if (Yii::$app->user->can('editProfile')) {
            if ($model->load(Yii::$app->request->post(), "Member")) {
                $model->save();
            }
            return $this->render('member_profile_admin', ['model' => $model, 'img_slides' => $this->img_slides, 'list_img' => $list_img]);
        }
        $data = Member::findById($member_id);
        return $this->render('member_profile', ['data' => $data, 'img_slides' => $this->img_slides, 'list_img' => $list_img]);
    }

}
