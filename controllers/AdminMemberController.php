<?php
namespace app\controllers;
use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\models\Role;
use app\models\Member;
use app\models\MemberSearch;
use app\helpers\MyHelper;

class AdminMemberController extends Controller
{
    public $layout = 'sbadmin/index';

    public function beforeAction($action) {
        if ($action->id != 'login'
            && $action->id != 'auth'
            &&(Yii::$app->user->getIsGuest() || MyHelper::isAdmin() == false)) {
            $this->redirect(Url::to(['admin/login']));
        } else
            return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $_GET = Yii::$app->request->get();
        if (!empty($_GET['search'])) {
            $searchModel = new MemberSearch();
            $dataProvider = $searchModel->search($_GET["MemberSearch"]);
        } else {
            $query = Member::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 50,
                ],
            ]);
        }

        if (!empty($_GET["apply-change"])) {
            $this->doApplyChange();
        }

        $roleObjs = Role::find()->all();
        $roleList = array();
        $roleList[''] = 'All';
        foreach ($roleObjs as $obj) {
            $roleList[$obj->id] = $obj->name;
        }

        $model = new MemberSearch();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'roleList' => $roleList,
        ]);
    }

    public function actionView($id)
    {
        $member = Member::findOne($id);
        return $this->render('view', ['member' => $member]);
    }

    public function actionUpdate($id)
    {

        $model = Member::findOne($id);
        $oldRoleName = Role::findOne($model->role_id)->name;
        $oldRole = Yii::$app->authManager->getRole($oldRoleName);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                $roleName = Role::findOne($model->role_id)->name;
                $role = Yii::$app->authManager->getRole($roleName);
                if($oldRole != null) Yii::$app->authManager->revoke($oldRole,  $id);
                Yii::$app->authManager->assign($role, $id);
                $this->redirect(Url::to(['admin-member/index']));
            } else {
                var_dump($model->errors);
                die();
            }
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    public function doApplyChange()
    {
        $_GET = Yii::$app->request->get();
        $bannedIds = $_GET['banned'];
        $checkedIds = array();
        $uncheckIds = array();
        foreach ($bannedIds as $id) {
            if ($id > 0) {
                $checkedIds[] = $id;
            } else {
                $uncheckIds[] = -$id;
            }
        }
        foreach ($uncheckIds as $key => $id) {
            if (in_array($id, $checkedIds))
                unset($uncheckIds[$key]);
        }
        foreach ($checkedIds as $id) {
            $model = Member::findOne(['id' => $id]);
            $model->is_banned = 1;
            $model->save();
        }
        foreach ($uncheckIds as $id) {
            $model = Member::findOne(['id' => $id]);
            $model->is_banned = 0;
            $model->save();
        }
        $this->redirect(Url::to(['admin-member/index']));
    }
}

?>

