<?php
    namespace app\controllers;
    use app\models\Image;
    use yii\data\ActiveDataProvider;
    use yii\web\Controller;
    use Yii;
    use app\helpers\MyHelper;
    use yii\web\UploadedFile;
    use yii\helpers\Url;

    class AdminImageController extends Controller{
        public $layout = 'sbadmin/index';
        public function beforeAction($action) {
            if ($action->id != 'login'
                && $action->id != 'auth'
                &&(Yii::$app->user->getIsGuest() || MyHelper::isAdmin() == false)) {
                $this->redirect(Url::to(['admin/login']));
            } else
                return parent::beforeAction($action);
        }

        public function actionIndex(){
            $query = Image::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'member_id' => SORT_ASC,
                    ]
                ]
            ]);
            return $this->render('index', ['dataProvider' => $dataProvider]);
        }

        public function actionAdd(){
            $model = new Image();
            if(isset($_POST['Image'])) {
                $file = UploadedFile::getInstanceByName('upload_img');
                $fileName = '';
                if($file != null){
                    $fileExtension = $file->getExtension();
                    $fileName = 'file_'.date('YmdHis').'.'.$fileExtension;
                    $fileUrl = Image::getUploadPath().$fileName;
                    $file->saveAs($fileUrl);
                }

                Yii::$app->db->createCommand()->insert('image',[
                    'member_id' => $_POST['Image']['member_id'],
                    'name' => $_POST['Image']['name'],
                    'image' => $fileName,
                ])->execute();
                $this->redirect(Url::to(['admin-image/index']));
            }
            return $this->render('add', ['model' => $model]);
        }

        public function actionUpdate($id){
            $model = Image::findOne($id);
            if(isset($_POST['Image'])){
                $file = UploadedFile::getInstanceByName('upload_img');
                if($file != null){
                    $fileExtension = $file->getExtension();
                    $fileName = 'file_'.$id.'_'.date('YmdHis').'.'.$fileExtension;
                    $fileUrl = Image::getUploadPath().$fileName;
                    $file->saveAs($fileUrl);
                    $model->img = $fileName;
                }
                $model->attributes = $_POST['Image'];
                if($model->save()){
                    $this->redirect(Url::to(['admin-image/index']));
                } else {
                    var_dump($model->getErrors());
                }
            }
            return $this->render('update', ['model' => $model]);
        }

        public function actionDelete($id){
            $model = Image::findOne($id);
            $model->delete();
            $this->redirect(Url::to(['admin-image/index']));
        }
    }
?>