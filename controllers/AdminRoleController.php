<?php
namespace app\controllers;

use app\models\Member;
use Yii;
use yii\web\Controller;
use app\models\Role;
use yii\helpers\Url;
use app\helpers\MyHelper;


class AdminRoleController extends Controller
{
    public $layout = 'sbadmin/index';

    public function beforeAction($action) {
        if ($action->id != 'login'
            && $action->id != 'auth'
            &&(Yii::$app->user->getIsGuest() || MyHelper::isAdmin() == false)) {
            $this->redirect(Url::to(['admin/login']));
        } else
            return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $dataProvider = Role::search();
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionAdd()
    {
        $model = new Role();
        $auth = Yii::$app->authManager;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $role = $auth->createRole($model->name);
                $role->description = $model->desc;
                $auth->add($role);
                $check = Yii::$app->request->post("check");
                if($check == null) $check = array();
                foreach ($check as $permissionName) {
                    $permission = $auth->getPermission($permissionName);
                    $auth->addChild($role, $permission);
                }
                $this->redirect(Url::to(['admin-role/index']));
            } else {
                var_dump($model->getErrors());
                die();
            }
        } else {
            $allPermissions = $auth->getPermissions();
            return $this->render('add', [
                'model' => $model,
                'allPermissions' => $allPermissions,
            ]);
        }
    }

    public function actionView($id)
    {
        $model = Role::findOne(['id' => $id]);
        if (!empty(Yii::$app->request->post())) {
            $_POST = Yii::$app->request->post();
            $auth = Yii::$app->authManager;
            $oldRoleName = $model->name;
            $role = $auth->getRole($oldRoleName);
            $newRoleName = Yii::$app->request->post('Role')['name'];
            $newRoleDesc = Yii::$app->request->post('Role')['desc'];
            // if name changed
            if ($newRoleName != $oldRoleName) {
                $role->name = $newRoleName;
                $model->name = $newRoleName;
                $auth->update($oldRoleName, $role);
                $role = $auth->getRole($newRoleName);
            }
            $model->desc = $newRoleDesc;
            $model->save();

            $check = $_POST['check'];
            $checked = array();
            $uncheck = array();
            foreach ($check as $key => $value) {
                if ($value > 0) {
                    $checked[] = $key;
                } else {
                    $uncheck[] = $key;
                }
            }
            foreach ($uncheck as $key => $value) {
                if (in_array($value . '_checked', $checked)) {
                    unset($uncheck[$key]);
                }
            }
            $checked = array();
            $permissions = $auth->getPermissions();
            foreach ($permissions as $permission) {
                if (in_array($permission->name, $uncheck) == false)
                    $checked[] = $permission->name;
            }

            foreach ($checked as $permissionName) {
                $permission = $auth->getPermission($permissionName);
                if ($auth->hasChild($role, $permission) == false) {
                    $auth->addChild($role, $permission);
                }
            }

            foreach ($uncheck as $permissionName) {
                $permission = $auth->getPermission($permissionName);
                if ($auth->hasChild($role, $permission) == true) {
                    $auth->removeChild($role, $permission);
                }
            }
            $auth->update($role->name, $role);
        }

        $auth = Yii::$app->authManager;
        $role = $auth->getRole($model->name);
        $permissions = $auth->getPermissionsByRole($role->name);
        $allPermissions = $auth->getPermissions();

        $permissionNames = array();
        foreach ($permissions as $per) {
            $permissionNames[] = $per->name;
        }
        return $this->render('view', [
            'model' => $model,
            'allPermissions' => $allPermissions,
            'permissionNames' => $permissionNames,
        ]);
    }

    public function actionDelete($id){
        $role = Yii::$app->authManager->getRole(Role::getRoleName($id));
        $model = Role::findOne($id);
        Member::updateAll(['role_id' => null], ['role_id' => $model->id]);
        $model->delete();
        Yii::$app->authManager->remove($role);
        $this->redirect(Url::to(['admin-role/index']));
    }
}

?>