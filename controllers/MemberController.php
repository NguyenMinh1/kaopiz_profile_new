<?php

namespace app\controllers;

use app\models\Role;
use yii\web\Controller;
use Yii;
use app\models\Member;
use yii\helpers\Url;
use app\helpers\MyHelper;
use yii\data\Pagination;
use app\models\Image;
use app\models\JoinTeam;
use yii\web\UploadedFile;

Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
    'css' => [],
    'js' => []
];

class MemberController extends Controller
{
    public $img_slides = '';
    public $defaultAction = 'all-members';
    private $pageSize = 40;


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();
        $email = $attributes['emails'][0]['value'];
        $name = isset($attributes['displayName']) ? $attributes['displayName'] : "";
        $member = Member::checkMember($email, $name);
        $member->login(3600 * 24 * 30);

        Yii::$app->session['email'] = $email;
        Yii::$app->session['name'] = $name;
        $member = Member::findByEmail($email);
        Yii::$app->session['id'] = $member->id;
        Yii::$app->session['role_id'] = $member->role_id;
        if (empty(Yii::$app->authManager->getRolesByUser($member->id))) {
            $roleName = Role::findOne($member->role_id)['name'];
            $role = Yii::$app->authManager->getRole($roleName);
            Yii::$app->authManager->assign($role, $member->id);
        }
        $this->redirect(Url::to(['/member/all-members']));
    }


    public function beforeAction($action)
    {
        $member=Member::findOne(['id'=>'18']);
        Yii::$app->user->login($member);
        if (Yii::$app->user->isGuest && $action->id != 'login')
            $this->redirect(Url::to(['/member/login']));
        $this->img_slides = Image::find()->asArray()->where(['member_id' => '-1'])->all();



        return parent::beforeAction($action);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->getIsGuest()) {
            $this->redirect(Url::to(['member/all-members']));
        }
        $model = new Member();
        return $this->render('login', ['model' => $model, 'img_slides' => $this->img_slides]);
    }

    public function actionAllMembers()
    {

        $pagination = new Pagination(['pageSize' => $this->pageSize]);
        if (Yii::$app->request->post('find')) {
            $name = Yii::$app->request->post('find');
            $members = Member::find()->asArray()->select(['id', 'name', 'phone', 'birthday', 'avatar'])->where(['like', 'name', $name])->orderBy('birthday,name');
        } else {
            $members = Member::find()->asArray()->select(['id', 'name', 'phone', 'birthday', 'avatar'])->orderBy('birthday,name');
        }
        if ($members->count() > 0) {
            $pagination->totalCount = $members->count();
            $members1 = $members->asArray()->offset($pagination->offset)->limit($pagination->limit)->all();
            return $this->render('list_member', ['members' => $members1, 'pagination' => $pagination, 'find' => '1', 'img_slides' => $this->img_slides, 'title' => 'All Members']);
        } else
            return $this->render('list_member', ['find' => '1', 'img_slides' => $this->img_slides, 'title' => 'All Members']);
    }

    public function actionOlderMembers()
    {
        $pagination = new Pagination(['pageSize' => $this->pageSize]);
        if ($birthday = Yii::$app->user->identity->birthday) {
            $members = Member::find()->asArray()->select(['id', 'name', 'phone', 'birthday', 'avatar'])->andWhere(['<', 'birthday', Yii::$app->user->identity->birthday])->andWhere(['not like', 'birthday', MyHelper::getYear($birthday)])->orderBy('birthday,name');
            if ($members->count() > 0) {
                $pagination->totalCount = $members->count();
                $members1 = $members->asArray()->offset($pagination->offset)->limit($pagination->limit)->all();
                return $this->render('list_member', ['members' => $members1, 'pagination' => $pagination, 'img_slides' => $this->img_slides, 'title' => 'Older Members']);
            }
        }
        return $this->render('list_member', ['img_slides' => $this->img_slides, 'title' => 'Older Members']);
    }

    public function actionMembersOfTheSameAge()
    {
        $pagination = new Pagination(['pageSize' => $this->pageSize]);
        if ($birthday = Yii::$app->user->identity->birthday) {
            $members = Member::find()->asArray()->select(['name', 'phone', 'birthday', 'avatar', 'id'])->andWhere(['like', 'birthday', MyHelper::getYear($birthday)])->andWhere(['<>', 'id', Yii::$app->user->id])->orderBy('birthday,name');
            if ($members->count() > 0) {
                $pagination->totalCount = $members->count();
                $members1 = $members->asArray()->offset($pagination->offset)->limit($pagination->limit)->all();
                return $this->render('list_member', ['members' => $members1, 'pagination' => $pagination, 'img_slides' => $this->img_slides, 'title' => 'Members of the same age']);
            }
        }
        return $this->render('list_member', ['img_slides' => $this->img_slides, 'title' => 'Members of the same age']);
    }

    public function actionYoungerMembers()
    {
        $pagination = new Pagination(['pageSize' => $this->pageSize]);
        if ($birthday = Yii::$app->user->identity->birthday) {
            $members = Member::find()->asArray()->select(['name', 'phone', 'birthday', 'avatar', 'id'])->andWhere(['>', 'birthday', Yii::$app->user->identity->birthday])->andWhere(['not like', 'birthday', MyHelper::getYear($birthday)])->orderBy('birthday,name');
            if ($members->count() > 0) {
                $pagination->totalCount = $members->count();
                $members1 = $members->asArray()->offset($pagination->offset)->limit($pagination->limit)->all();
                return $this->render('list_member', ['members' => $members1, 'pagination' => $pagination, 'img_slides' => $this->img_slides, 'title' => 'Younger Members']);
            }
        }

        return $this->render('list_member', ['img_slides' => $this->img_slides, 'title' => 'Younger Members']);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['/member/login']);
    }

    public function actionJoinTeam($team_id)
    {
        $member_id = Yii::$app->user->id;
        $join_team = JoinTeam::findOne(['member_id' => $member_id, 'team_id' => $team_id]);
        if ($join_team == null) {
            $join_team = new JoinTeam();
            $join_team->member_id = $member_id;
            $join_team->team_id = $team_id;
            $join_team->active = 0;
            $join_team->save();
        } else {
            if ($join_team->active != 1)
                $join_team->active = 0;
            $join_team->save();
        }
        $this->redirect(['/team/team']);
    }


    public function actionLeaveTeam($team_id)
    {
        $member_id = Yii::$app->user->id;
        $join_team = JoinTeam::findOne(['member_id' => $member_id, 'team_id' => $team_id]);
        $join_team->active = -1;
        $join_team->save();
        $this->redirect(['/team/team']);

    }

    public function actionUpload()
    {

        $model = new Image();
        if ($model->load(Yii::$app->request->post())) {
            $model->crop_info = $model->image;
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image->saveAs('web/img/' . $model->crop_info . '-' . date('_Y_m_d_h_i_s_a') . '.' . $model->image->extension);
            $name = $model->image = $model->crop_info . '-' . date('_Y_m_d_h_i_s_a') . '.' . $model->image->extension;
            $model->member_id = Yii::$app->user->id;
            if (!$model->save(false)) {
                echo 'Error!';
                usleep('5000000');
            } else {
                if ($model->is_avatar) {
                    $member = Member::findOne(['id' => Yii::$app->user->id]);
                    $member->avatar = $name;
                    $member->save(false);
                }
                $this->redirect(['/profile/my-profile']);
            }
        }
        return $this->render('upload', ['model' => $model, 'img_slides' => $this->img_slides]);
    }

    public function actionDeleteImg($image)
    {
        $img = Image::findOne(['image' => $image]);
        if ($img) {
            $img->delete();
            if (file_exists(Yii::getAlias('@web/web/img/' . $image)))
                unlink(Yii::getAlias('@web/web/img/' . $image));
        }

        $member = Member::findOne(['id' => Yii::$app->user->id]);
        if ($member->avatar == $image) {
            $member->avatar = '';
            $member->save(false);
        }
        $this->redirect(['/profile/my-profile']);
    }


}
