<?php 
	namespace app\controllers;
	use Yii;
	use yii\filters\AccessControl;
	use yii\web\Controller;
	use yii\filters\VerbFilter;
	use app\models\LoginForm;
	use app\models\Member;
	use app\models\Team;
	use app\models\JoinTeam;
	use app\models\User;
	use yii\helpers\Url;
	use yii\helpers\Html;
	use app\helpers\MyHelper;
	use yii\data\ActiveDataProvider;
	class AdminTeamController extends Controller{
		public $layout = 'sbadmin/index';

		public function beforeAction($action) {
			if ($action->id != 'login'
				&& $action->id != 'auth'
				&&(Yii::$app->user->getIsGuest() || MyHelper::isAdmin() == false)) {
				$this->redirect(Url::to(['admin/login']));
			} else
				return parent::beforeAction($action);
		}
		
		public function actionIndex(){

			$query = Team::find();
			$dataProvider = new ActiveDataProvider([
				"query" => $query,
				"pagination" => [
					"pageSize" => 50,
				],
			]);
			return $this->render('index', ['dataProvider' => $dataProvider]);
		}

		public function actionCreate(){
			$model = new Team();
			if($model->load(Yii::$app->request->post())){
				$model->save();
				$this->redirect(Url::to(['admin-team/index']));
			} else {
				return $this->render('create', ['model' => $model]);
			}
		}

		public function actionView($id){
			$team = Team::findOne($id);
			return $this->render('view', ['team' => $team]);
		}

		public function actionUpdate($id){
			$team = Team::findOne($id);
			if($team->load(Yii::$app->request->post())){
				if($team->save()){
					$this->redirect(Url::to(['admin-team/index']));
				} else {
					var_dump($team->getErrors());
					die();
				}
			}
			$members = JoinTeam::getMemberOfTeam($id);
			$member_ids = array();
			foreach ($members as $member){
				$member_ids[] = $member->id;
			}
			$query = Member::find()->where(['in', 'id', $member_ids]);
			$memberProvider = new ActiveDataProvider([
				"query" => $query,
				"pagination" => [
					"pageSize" => 50,
				],
			]);

			return $this->render('update', ['model' => $team, 'memberProvider' => $memberProvider]);
		}
		
		public function actionAddMember($member_id, $team_id){
			$join_team = JoinTeam::findOne(['member_id' => $member_id, 'team_id' => $team_id]);
			$join_team->active = 1;
			$join_team->save();
			$this->redirect(Url::to(['notification/index']));
		}

		public function actionRemoveMember($member_id, $team_id){
			$joinTeam = JoinTeam::findOne(['team_id' => $team_id, 'member_id' => $member_id, 'active' => 1]);
			if($joinTeam != null) $joinTeam->delete();
			$this->redirect(Url::to(['team/update?id='.$team_id]));
		}

		public function actionRemoveJoinRequest($member_id, $team_id){
			$join_team = JoinTeam::findOne(['member_id' => $member_id, 'team_id' => $team_id]);
			if($join_team != null) $join_team->delete();
			$this->redirect(Url::to(['notification/index']));
		}

		public function actionRemoveOutRequest($member_id, $team_id){
			$join_team = JoinTeam::findOne(['member_id' => $member_id, 'team_id' => $team_id]);
			$join_team->active = 1;
			$join_team->save();
			$this->redirect(Url::to(['notification/index']));
		}

		public function actionDelete($id){
			$team = Team::findOne($id);
			if($team != null){
				JoinTeam::deleteAll(['team_id' => $id]);
				$team->delete();
			}
			$this->redirect(Url::to(['admin-team/index']));
		}


	}
 ?>