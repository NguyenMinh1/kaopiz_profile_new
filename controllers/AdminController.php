<?php 
	namespace app\controllers;
	use Yii;
	use yii\helpers\Url;
	use yii\web\Controller;
	use app\models\Member;
	use app\helpers\MyHelper;
	class AdminController extends Controller{
		public $layout = 'admin';

		public function beforeAction($action) {
	        if ($action->id != 'login'
				&& $action->id != 'auth'
				&&(Yii::$app->user->getIsGuest() || MyHelper::isAdmin() == false)) {
	            $this->redirect(Url::to(['admin/login']));
	        } else
	        return parent::beforeAction($action);
	    }

	    public function actions()
	    {
	        return [
	            'error' => [
	                'class' => 'yii\web\ErrorAction',
	            ],
	            'captcha' => [
	                'class' => 'yii\captcha\CaptchaAction',
	                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
	            ],
	            'auth' => [
	                'class' => 'yii\authclient\AuthAction',
	                'successCallback' => [$this, 'successCallback'],
	            ],
	        ];
	    }

	    public function successCallback($client)
	    {
	    	$attributes = $client->getUserAttributes();
		    $email = $attributes['emails'][0]['value'];
		    $name = isset($attributes['displayName'])? $attributes['displayName']:""; 
		    $member = Member::checkMember($email, $name);
	    	$member->login(0);   
		    Yii::$app->session['email'] = $email;
		    Yii::$app->session['name'] = $name;
		    $member = Member::findByEmail($email);
		    Yii::$app->session['id'] = $member->id; 
		    Yii::$app->session['role_id'] = $member->role_id;
		    $this->redirect(Url::to(['admin/index']));
	    }
		
		public function actionLogin(){
			$model = new Member();
			return $this->render('login', ['model' => $model]);
		}

		public function actionLogout(){
			Yii::$app->user->logout();
			$this->redirect(Url::to(['admin/login']));
		}

		public function actionIndex(){
			$this->layout = 'sbadmin/index';
			return $this->render('index');
		}
	}
 ?>