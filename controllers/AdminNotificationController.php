<?php
    namespace app\controllers;

    use yii\web\Controller;
    use app\models\JoinTeam;
    use yii\helpers\Url;
    use app\helpers\MyHelper;
    use Yii;

    class AdminNotificationController extends Controller{
        public $layout = 'sbadmin/index';

        public function beforeAction($action) {
            if ($action->id != 'login'
                && $action->id != 'auth'
                &&(Yii::$app->user->getIsGuest() || MyHelper::isAdmin() == false)) {
                $this->redirect(Url::to(['admin/login']));
            } else
                return parent::beforeAction($action);
        }
        public function actionIndex(){
            $join_teams = JoinTeam::find()
                ->where(['active' => 0])
                ->orderBy('team_id')
                ->all();
            $out_teams = JoinTeam::find()
                ->where(['active' => -1])
                ->orderBy('team_id')
                ->all();
            return $this->render('index', ['join_teams' => $join_teams, 'out_teams' => $out_teams]);
        }
    }
?>