<?php 
	namespace app\controllers;
	use yii\console\Controller;
	use Yii;

	class RabcController extends Controller{
		public function actionInit(){
			$auth = Yii::$app->authManager;
			
			//add 'edit Profile' Permission 
			$editProfile = $auth->createPermission('editProfile');
			$editProfile->description = 'edit Profile';
			$auth->add($editProfile);

			//add 'edit Own Profile' Permission
			$editOwnProfile = $auth->createPermission('editOwnProfile');
			$editOwnProfile->description = 'edit Own Profile';
			$rule = new \app\rabc\EditOwnProfileRule();
			$auth->add($rule);
			$editOwnProfile->ruleName = $rule->name;
			$auth->add($editOwnProfile);
			$auth->addChild($editOwnProfile, $editProfile);

			//add 'Create Team' permission
			$createTeam = $auth->createPermission('createTeam');
			$createTeam->description = 'Create Team';
			$auth->add($createTeam);

			//add 'Edit Team' permission
			$editTeam = $auth->createPermission('editTeam');
			$editTeam->description = 'Edit Team';
			$auth->add($editTeam);

			//add 'Delete Team' permission
			$deleteTeam = $auth->createPermission('deleteTeam');
			$deleteTeam->description = 'Delete Team';
			$auth->add($deleteTeam);

			//add 'Add Member to Team' permission
			$addMember = $auth->createPermission('addMember');
			$addMember->description = 'Add Member';
			$auth->add($addMember);

			//add 'Remove Member form Team' permission
			$removeMember = $auth->createPermission('removeMember');
			$removeMember->description = 'Remove Member';
			$auth->add($removeMember);

			// //add 'create Role' permission
			// $createRole = $auth->createPermission('createRole');
			// $createRole->description = 'Create Role';
			// $auth->add($createRole);

			// //add 'edit Role' permission
			// $editRole = $auth->createPermission('editRole');
			// $editRole->description = 'Edit Role';
			// $auth->add($editRole);

			// //add 'delete Role' permission
			// $deleteRole = $auth->createPermission('deleteRole');
			// $deleteRole->description = 'Delete Role';
			// $auth->add($deleteRole);

			//add 'member' Role
			$member = $auth->createRole('member');
			$auth->add($member);
			$auth->addChild($member, $editOwnProfile);

			//add 'admin' Role
			$admin = $auth->createRole('admin');
			$auth->add($admin);
			$auth->addChild($admin, $editProfile);
			$auth->addChild($admin, $member);
			$auth->addChild($admin, $createTeam);
			$auth->addChild($admin, $editTeam);
			$auth->addChild($admin, $deleteTeam);
			$auth->addChild($admin, $addMember);
			$auth->addChild($admin, $removeMember);
			$auth->addChild($admin, $createRole);
			$auth->addChild($admin, $editRole);
			$auth->addChild($admin, $deleteRole);	
		}
	}

 ?>