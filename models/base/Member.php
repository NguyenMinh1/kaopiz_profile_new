<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $sex
 * @property integer $avatar
 * @property string $birthday
 * @property string $address
 * @property string $phone
 * @property integer $height
 * @property integer $weight
 * @property string $country
 * @property string $hobby
 * @property string $forte
 * @property string $foible
 * @property string $nickname
 * @property string $marital
 * @property string $sky
 * @property string $facebook
 * @property integer $role_id
 * @property integer $is_banned
 */
class Member extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            //[['email', 'is_banned'], 'required'],
            [['avatar', 'height', 'weight', 'role_id', 'is_banned'], 'integer'],
            [['birthday'], 'safe'],
            [['address', 'hobby', 'forte', 'foible', 'marital'], 'string'],
            [['email', 'name', 'country', 'nickname', 'sky', 'facebook'], 'string', 'max' => 100],
            [['sex'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
            'sex' => 'Sex',
            'avatar' => 'Avatar',
            'birthday' => 'Birthday',
            'address' => 'Address',
            'phone' => 'Phone',
            'height' => 'Height',
            'weight' => 'Weight',
            'country' => 'Country',
            'hobby' => 'Hobby',
            'forte' => 'Forte',
            'foible' => 'Foible',
            'nickname' => 'Nickname',
            'marital' => 'Marital',
            'sky' => 'Sky',
            'facebook' => 'Facebook',
            'role_id' => 'Role ID',
            'is_banned' => 'Is Banned',
        ];
    }

}
