<?php

namespace app\models;

use app\models\JoinTeam;
use Yii;
use app\models\Member;
class Team extends base\Team {

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Team Name',
            'desc' => 'Team Description',
        ];
    }

    public static function getRequestTeam(){
        return Team::find()->where(['active' => [0, -1]])->count();
    }
}

?>