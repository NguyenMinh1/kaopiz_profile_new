<?php

namespace app\models;
use app\models\Member;
use Yii;

/**
 * This is the model class for table "join_team".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $team_id
 * @property integer $active: trạng thái join(3 trạng thái)
  -1: Là thành viên của nhóm nhưng Yêu cầu rời team
  0:  Yêu cầu tham gia vào team
  1:  Là thành viên của team
 */
class JoinTeam extends base\JoinTeam {

    public static function isMemberOfTeam($member_id, $team_id) {
        $mem = JoinTeam::findOne(['member_id' => $member_id, 'team_id' => $team_id, 'active' => [1, -1]]);
        if ($mem != null)
            return true;
        return false;
    }

    // Number Request: Join Team OR Out Team
    public static function getNumberNofi() {
        $count = JoinTeam::find()->where(['active' => [0, -1]])->count();
        return $count;
    }

    public function getMember() {
        return $this->hasOne(Member::className(),['id'=>'member_id']);
    }

    public static function getMemberOfTeam($team_id){
        $joinTeams = JoinTeam::find()->where(['team_id' => $team_id])->andWhere(["active" => 1])->all();
        $members = array();
        if($joinTeams != null){
            foreach ($joinTeams as $joinTeam){
                $member = Member::findOne(['id' => $joinTeam->member_id]);
                if($member != null) $members[] = $member;
            }
        }
        return $members;
    }

}

?>