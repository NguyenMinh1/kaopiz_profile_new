<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=kaopiz_profile',
    'username' => 'root',
    'password' => 'password',
    'charset' => 'utf8',
];
