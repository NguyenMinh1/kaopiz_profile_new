<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'defaultRoute' => 'member',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    //Edit

//    'as beforeRequest' => [
//        'class' => 'yii\filters\AccessControl',
//        'rules' => [
//            [
//                'allow' => true,
//                'actions' => ['login'],
//            ],
//            [
//                'allow' => true,
//                'roles' => ['@'],
//            ],
//        ],
//        'denyCallback' => function () {
//            return Yii::$app->response->redirect(['member/login']);
//        },
//    ],
    'layout' => 'layout',

    //End edit
    'components' => [
        //Edit
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],

        'BeforeRequest' => [
            'class' => 'app\components\BeforeRequest',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                    'clientId' => '590249975870-cee4t33ghqi4jemsfundgbeapjm66575.apps.googleusercontent.com',
                    'clientSecret' => 'dR_IqpGRpoF5ZoO2QdTfwK4f',
                ],
            ],
        ],


        //End edit
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'asdfsadfgsdfgdfgretr',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Member',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'member/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'all-members' => 'member/all-members',
                'older-members' => 'member/older-members',
                'younger-member' => 'member/younger-members',
                'members-of-the-same-age' => 'member/members-of-the-same-age',
                'team' => 'team/team',
                'my-profile' => 'profile/my-profile',
                'member-profile' => 'profile/member-profile',
                'login' => 'member/login',
                'upload'=>'member/upload',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
