<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LayoutAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'web/css/animate.css',
        'web/css/bootstrap.min.css',
        'web/css/font-awesome.min.css',
        'web/css/normalize.min.css',
        'web/css/templatemo_misc.css',
        'web/css/templatemo_style.css'
    ];
    public $js = [
//        'web/js/bootstrap.js',
//        'web/js/jquery.easing-1.3.js',
//        'web/js/main.js',
//        'web/js/plugin.js',
//        'web/js/vendor/jquery-1.10.1.min.js',
//        'web/js/vendor/modernizr-2.6.2.min.js',
//        'web/js/vendor/jquery.gmap3.min.js'
    ];
    public $depends = [
            //'yii\web\YiiAsset',
            //'yii\bootstrap\BootstrapAsset',
    ];

}
