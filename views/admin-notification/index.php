<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Member;
use app\models\Team;

?>
    <div class="title">
        <b>Notification</b>
    </div>
<?php
displayAll($join_teams, $out_teams);
?>

<?php
function displayJoinTeam($member, $team)
{
    echo Html::tag('div',
        Html::tag('p',
            Html::a($member->name, Url::to(['admin-member/view?id=' . $member->id])) .
            ' yêu cầu tham gia vào team ' .
            Html::a($team->name, Url::to(['admin-team/view?id=' . $team->id]))) .
        Html::a('Cho phép', Url::to(['admin-team/add-member?member_id=' . $member->id . '&team_id=' . $team->id]), [
            'class' => 'btn btn-primary'
        ]) . ' ' .
        Html::a('Xóa yêu cầu', Url::to(['admin-team/remove-join-request?member_id=' . $member->id . '&team_id=' . $team->id]), [
            'class' => 'btn btn-warning'
        ]) .
        Html::tag('hr')
    );
}

function displayOutTeam($member, $team)
{
    echo Html::tag('div',
        Html::tag('p',
            Html::a($member->name, Url::to(['admin-member/view?id=' . $member->id])) .
            ' yêu cầu rời khỏi team ' .
            Html::a($team->name, Url::to(['admin-team/view?id=' . $team->id]))) .
        Html::a('Cho phép', Url::to(['admin-team/remove-member?member_id=' . $member->id . '&team_id=' . $team->id]), [
            'class' => 'btn btn-primary'
        ]) . ' ' .
        Html::a('Xóa yêu cầu', Url::to(['admin-team/remove-out-request?member_id=' . $member->id . '&team_id=' . $team->id]), [
            'class' => 'btn btn-warning'
        ]) .
        Html::tag('hr')
    );
}

function displayAll($join_teams, $out_teams)
{
    foreach ($join_teams as $join_team) {
        $team = Team::findOne(['id' => $join_team->team_id]);
        $member = Member::findOne(['id' => $join_team->member_id]);
        displayJoinTeam($member, $team);
    }

    foreach ($out_teams as $out_team) {
        $team = Team::findOne(['id' => $out_team->team_id]);
        $member = Member::findOne(['id' => $out_team->member_id]);
        displayOutTeam($member, $team);
    }
}

?>