<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
    <div class="title">
        <b>Update Team</b>
    </div>
<?php
    $form = ActiveForm::begin();
    echo $form->field($model, 'name');
    echo $form->field($model, 'desc');
    Yii::$app->session->setFlash('team_id', $model->id);
    echo "<b>Members of Team</b>";
    echo \yii\grid\GridView::widget([
        'dataProvider' => $memberProvider,
        'caption' => 'Team '.$model['name'].'\'s Members',
        "columns" => [
            'name',
            'email',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function($model, $key, $index){
                        $options = [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to remove this member?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash">Remove</span>',
                                        Url::to(['admin-team/remove-member?member_id='.$key->id.'&team_id='.Yii::$app->session["team_id"]]),
                                        ['onclick' => 'return checkRemoveMember()']);
                    }
                ],
            ],

        ],
    ]);
    echo Html::submitButton('Update', ['class' => 'btn btn-primary']);
    echo " ";
    echo Html::a('Cancle',Url::to(['admin-team/index']) ,['class' => 'btn btn-warning']);
    ActiveForm::end();
?>