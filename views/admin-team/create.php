<?php
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\helpers\Url;
?>
    <div class="title">
        <b>Create Team</b>
    </div>
<?php
$form = ActiveForm::begin();
echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'desc')->textarea(['rows' => 10]);
echo Html::submitButton('Create Team',['class' => 'btn btn-primary']);
echo ' ';
echo Html::a('Cancle',Url::to(['admin-team/index']), ['class' => 'btn btn-warning']);
ActiveForm::end();
?>