<?php
use yii\helpers\Html;
?>
<div class="title">
    <b>Team Information</b>
</div>
<h3>Team Information</h3>
<p><b>Team Name:</b> <?php echo $team->name;?></p>
<p><b>Description:</b> <?php echo $team->desc;?></p>
<?php echo Html::button('Go Back', ['onclick' => 'history.go(-1)', 'class' => 'btn btn-primary']); ?>
