<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\MyUrl;
use app\models\Member;
use app\models\JoinTeam;
use yii\grid\GridView;

?>
<div class="title">
    <b>Teams Information</b>
</div>
<?php
echo Html::a('+ Add Team', Url::to(['admin-team/create']), ['class' => 'btn btn-primary']);
?>

<?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'caption' => 'Kaopiz\'s Teams',
        'columns' => [
            'id',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'delete' => function($model, $key, $index){
                        $options = [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to remove this Role?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash">Remove</span>',
                            Url::to(['admin-team/delete?id='.$key->id]),
                            ['onclick' => 'return checkDeleteTeam()']);
                    }
                ],
            ],
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'desc',
                'label' => 'Description'
            ],

        ]
    ]);
?>

