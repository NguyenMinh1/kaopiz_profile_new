<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use app\helpers\MyUrl;
use app\models\JoinTeam;
use app\assets\AdminAsset;
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kaopiz's Profile Admin</title>
    <!-- CSS My Admin -->
    <link href="/kaopiz_profile_new/web/css/admin.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="/kaopiz_profile_new/views/layouts/sbadmin/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/kaopiz_profile_new/views/layouts/sbadmin/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/kaopiz_profile_new/views/layouts/sbadmin/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/kaopiz_profile_new/views/layouts/sbadmin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= Url::to(["admin/index"]);?>">Kaopiz's Profile Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo !empty(Yii::$app->session['name'])? Yii::$app->session['name']:  'Admin'?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= Url::to(["admin-member/view?id=".Yii::$app->user->id]);?>"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?= Url::to(["admin/logout"]);?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?= Url::to(["admin/index"]);?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="<?= Url::to(["admin-member/index"]);?>"><i class="fa fa-users"></i> Member</a>
                    </li>
                    <li>
                        <a href="<?= Url::to(["admin-team/index"]);?>"><i class="fa fa-fw fa-table"></i> Team</a>
                    </li>
                    <li>
                        <a href="<?= Url::to(["admin-role/index"]);?>"><i class="fa fa-fw fa-edit"></i> Role</a>
                    </li>
                    <li>
                        <a href="<?= Url::to(["admin-image/index"]);?>"><i class="fa fa-fw fa-image"></i> Images</a>
                    </li>
                    <li>
                        <a href="<?= Url::to(["admin-notification/index"]);?>"><i class="fa fa-bell"></i><?php echo JoinTeam::getNumberNofi()!=0? ' Notification('.JoinTeam::getNumberNofi().')':' Notification'?></a>
                    </li>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                
                <div id="my-admin-content">
                    <?= $content?>
                </div>


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script src="/kaopiz_profile_new/web/js/admin.js"></script>
    <!-- jQuery -->
    <script src="/kaopiz_profile_new/views/layouts/sbadmin/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/kaopiz_profile_new/views/layouts/sbadmin/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="/kaopiz_profile_new/views/layouts/sbadmin/js/plugins/morris/raphael.min.js"></script>
    <script src="/kaopiz_profile_new/views/layouts/sbadmin/js/plugins/morris/morris.min.js"></script>
    <script src="/kaopiz_profile_new/views/layouts/sbadmin/js/plugins/morris/morris-data.js"></script>

</body>

</html>
