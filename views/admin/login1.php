<?php 
 	use yii\widgets\ActiveForm;

	$form = ActiveForm::begin();
?>
<div class='col-50'><?= $form->field($model, 'username')->textInput(); ?></div>
<div class='col-50'><?= $form->field($model, 'password')->passwordInput(); ?></div>
<?= yii\authclient\widgets\AuthChoice::widget([
	     'baseAuthUrl' => ['admin/auth'],
	     'popupMode' => false,
		]);
?>