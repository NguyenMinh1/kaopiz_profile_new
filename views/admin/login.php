<?php
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
?>
<!-- start: Meta -->
<meta charset="utf-8">
<meta name="description" content="Bootstrap Metro Dashboard">
<meta name="author" content="Dennis Ji">
<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
<!-- end: Meta -->

<!-- start: Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- end: Mobile Specific -->

<!-- start: CSS -->
<link id="bootstrap-style" href="/kaopiz_profile_new/views/layouts/sbadmin/metro/css/bootstrap.min.css" rel="stylesheet">
<link href="/kaopiz_profile_new/views/admin/metro/css/bootstrap-responsive.min.css" rel="stylesheet">
<link id="base-style" href="/kaopiz_profile_new/views/admin/metro/css/style.css" rel="stylesheet">

<link
    href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext'
    rel='stylesheet' type='text/css'>
<!-- end: CSS -->


<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<link id="ie-style" href="/kaopiz_profile_new/views/admin/metro/css/ie.css" rel="stylesheet">
<![endif]-->

<!--[if IE 9]>
<link id="ie9style" href="/kaopiz_profile_new/views/admin/metro/css/ie9.css" rel="stylesheet">
<![endif]-->

<!-- start: Favicon -->
<link rel="shortcut icon" href="/kaopiz_profile_new/views/admin/metro/img/favicon.ico">
<!-- end: Favicon -->

<style type="text/css">
    body {
        /*background-image: url(/kaopiz_profile_new/views/admin/metro/img/bg-login.jpg) !important;*/
    }
</style>


<div class="container-fluid-full">
    <div class="row-fluid">

        <div class="row-fluid">
            <div class="login-box">

                <h2>Login to your account</h2>
                <form class="form-horizontal" action="index.html" method="post">
                    <fieldset>

                        <div class="input-prepend" title="Username">
                            <input class="input-large span10" name="username" id="username" type="text"
                                   placeholder="Username"/>
                        </div>
                        <div class="clearfix"></div>

                        <div class="input-prepend" title="Password">
                            <input class="input-large span10" name="password" id="password" type="password"
                                   placeholder="Password"/>
                        </div>
                        <div class="clearfix"></div>

                        <label class="remember" for="remember"><input type="checkbox" id="remember"/>Remember me</label>
                        <div class="button-login">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                        <div class="clearfix"></div>
                </form>
                <div style="padding-left: 170px">
                    <?= yii\authclient\widgets\AuthChoice::widget([
                        'baseAuthUrl' => ['admin/auth'],
                        'popupMode' => false,
                    ]);
                    ?>
                </div>

            </div><!--/span-->
        </div><!--/row-->


    </div><!--/.fluid-container-->

</div><!--/fluid-row-->

<?php
    ActiveForm::end();
?>

