<?php

use app\helpers\MyHelper;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


?>
<title><?= $title ?></title>
<div id="menu-1" class="about content">
    <div class="row">
        <ul class="tabs">
            <li class="col-md-3 col-sm-3">
                <a href=<?= Url::to(['/member/all-members']) ?> class="icon-item <?php if (Yii::$app->controller->action->id == 'all-members') echo 'active' ?>
                ">
                <b style="font-size:15px">All<br>Members</b>
                </a> <!-- /.icon-item -->
            </li>
            <li class="col-md-3 col-sm-3">
                <a href=<?= Url::to(['/member/older-members']) ?> class="icon-item <?php if (Yii::$app->controller->action->id == 'older-members') echo 'active' ?>
                ">
                <b style="font-size:15px">Older<br>Members</b>
                </a> <!-- /.icon-item -->
            </li>
            <li class="col-md-3 col-sm-3">
                <a href=<?= Url::to(['/member/members-of-the-same-age']) ?> class="icon-item <?php if (Yii::$app->controller->action->id == 'members-of-the-same-age') echo 'active' ?>
                ">
                <b style="font-size:15px">Members Of The Same Age</b>
                </a> <!-- /.icon-item -->
            </li>
            <li class="col-md-3 col-sm-3">
                <a href=<?= Url::to(['/member/younger-members']) ?> class="icon-item <?php if (Yii::$app->controller->action->id == 'younger-members') echo 'active' ?>
                ">
                <b style="font-size:15px">Younger<br>Members</b>
                </a> <!-- /.icon-item -->
            </li>
        </ul>



    </div>
    <!-- /.row -->
    <?php if (isset($find)): ?>
    <?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-6 col-sm-6"></div>
        <div class="col-md-4 col-sm-4">
            <input name="find" placeholder="Find member...">
        </div>
        <div class="col-md-2 col-sm-2">
            <input type="submit" value="Find"></div>
    </div>
    <?php ActiveForm::end() ?>
</div>
<?php endif; ?>

<div class="row">

    <?php if (isset($members)) { ?>
        <?php foreach ($members as $member): ?>

            <div class="col-md-3 col-sm-3">
                <div class="member-item">
                    <div class="thumb">
                        <a href=<?= Url::to(['/profile/member-profile', 'member_id' => $member['id']]) ?>><img
                                src=<?php if(empty($member['avatar'])) echo Yii::getAlias('@web/web/images/base_avatar.jpg');
                            else echo Yii::getAlias('@web/web/img/'.$member['avatar']);
                            ?>></a>
                    </div>
                    <h4><?= $member['name'] ?></h4>
                    <span>Age: </span><?php if ($member['birthday']) echo MyHelper::getAge($member['birthday']); else echo NONE; ?>
                    <br>
                    <span>Phone Numbers: </span><?php if ($member['phone']) echo $member['phone']; else echo NONE; ?>
                </div>
                <!-- /.member-item -->
            </div>

        <?php endforeach; ?>
        <div class="col-md-12 col-sm-12"><?= LinkPager::widget(['pagination' => $pagination]) ?></div>
    <?php }  ?>

</div>
</div>
<?php if($img_slides){?>
<div id="slideshowxx">
    <img class="activexx" src=<?= Yii::getAlias('@web/web/img/' . $img_slides[0]['image']) ?>>
    <?php for ($i = 1; $i < count($img_slides); $i++) { ?>
        <img src=<?= Yii::getAlias('@web/web/img/' . $img_slides[$i]['image']) ?>>
    <?php } ?>
</div>
<?php }?>

<script type="text/javascript" src=<?= Yii::getAlias('@web/web/js/vendor/jquery-1.10.1.min.js') ?>></script>
<script type="text/javascript">

    function slideSwitch() {
        var $active = $('#slideshowxx IMG.activexx');

        if ($active.length == 0) $active = $('#slideshowxx IMG:last');

        // use this to pull the images in the order they appear in the markup
        var $next = $active.next().length ? $active.next()
            : $('#slideshowxx IMG:first');

// xóa ghi chú ở 3 dòng bên dưới nếu bạn muốn hiển thị ảnh ngẫu nhiên

        var $sibs = $active.siblings();
        var rndNum = Math.floor(Math.random() * $sibs.length);
        var $next = $($sibs[rndNum]);

        $active.addClass('last-activexx');

        $next.css({opacity: 0.0})
            .addClass('activexx')
            .animate({opacity: 1.0}, 1000, function () {
                $active.removeClass('activexx last-activexx');
            });
    }

    $(function () {
        setInterval("slideSwitch()", 10000);
    });

</script>
<style>
    #slideshowxx {
        position: relative;
        height: 350px;
        z-index: -1;
    }

    #slideshowxx IMG {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 8;
        opacity: 0.0;
    }

    #slideshowxx IMG.activexx {
        z-index: 10;
        opacity: 1.0;
    }

    #slideshowxx IMG.last-activexx {
        z-index: 9;
    }

    #slideshowxx img {
        /* Set rules to fill background */
        min-height: 100%;
        min-width: 1024px;

        /* Set up proportionate scaling */
        width: 100%;
        height: auto;

        /* Set up positioning */
        position: fixed;
        top: 0;
        left: 0;
    }

</style>
