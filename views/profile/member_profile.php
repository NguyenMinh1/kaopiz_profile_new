<?php

?>
<title><?= $data['name'] ?></title>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="box_img">
            <img id="img" src=<?php if ($list_img) echo Yii::getAlias('@web/web/img/' . $list_img[0]['image']);
            else echo Yii::getAlias('@web/web/images/base_img.jpg');
            ?>>
            <div class="row">
                <div class="col-md-4 col-sm-4"></div>
                <div class="col-md-2 col-sm-2">
                    <div class="prev"></div>
                </div>
                <div class="col-md-2 col-sm-2">
                    <div class="next"></div>
                </div>
                <div class="col-md-4 col-sm-4"></div>
            </div>
        </div>
        <div class="profile">
            <table>
                <tr>
                    <td class="title">Name:</td>
                    <td><?= $data['name'] ? $data['name'] : NONE ?></td>
                    <td class="title">&nbsp &nbsp Sex:</td>
                    <td><?= $data['sex'] ? $data['sex'] : NONE ?></td>
                </tr>
                <tr>
                    <td class="title">Birthday:</td>
                    <td><?= $data['birthday'] ? $data['birthday'] : NONE ?></td>
                    <td class="title">&nbsp &nbsp Email:</td>
                    <td><?= $data['email'] ? $data['email'] : NONE ?></td>
                </tr>
                <tr>
                    <td class="title">Address:</td>
                    <td><?= $data['address'] ? $data['address'] : NONE ?></td>
                    <td class="title">&nbsp &nbsp Phone Numbers:</td>
                    <td><?= $data['phone'] ? $data['phone'] : NONE ?></td>
                </tr>
                </tr>
                <tr>
                    <td class="title">Height:</td>
                    <td><?= $data['height'] ? $data['height'] . '(Cm)' : NONE ?></td>
                    <td class="title">&nbsp &nbsp Weight:</td>
                    <td><?= $data['weight'] ? $data['weight'] . '(Kg)' : NONE ?></td>
                </tr>
                </tr>
                <tr>
                    <td class="title">Country:</td>
                    <td><?= $data['country'] ? $data['country'] : NONE ?></td>
                    <td class="title">&nbsp &nbsp Nickname:</td>
                    <td><?= $data['nickname'] ? $data['nickname'] : NONE ?></td>
                </tr>
                </tr>
                <tr>
                    <td class="title">Sky:</td>
                    <td><?= $data['sky'] ? $data['sky'] : NONE ?></td>
                    <td class="title">&nbsp &nbsp Facebook:</td>
                    <td><?php if ($data['facebook']) { ?><a href=<?= $data['facebook'] ?> target="new">
                                Facebook</a><?php } else echo NONE; ?></td>
                </tr>
                <tr>
                    <td class="title">Marital:</td>
                    <td colspan="3"><?= $data['marital'] ? $data['marital'] : NONE ?></td>
                </tr>
                <tr>
                    <td class="title">Hobby:</td>
                    <td colspan="3"><?= $data['hobby'] ? $data['hobby'] : NONE ?></td>
                </tr>
                <tr>
                    <td class="title">Forte:</td>
                    <td colspan="3"><?= $data['forte'] ? $data['forte'] : NONE ?></td>
                </tr>
                <tr>
                    <td class="title">Foible:</td>
                    <td colspan="3"><?= $data['foible'] ? $data['foible'] : NONE ?></td>
                </tr>
                <tr align='left'>
                    <td colspan="2"></td>
                    <td colspan="2" id="tr_submit"></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div id="slideshowxx">
    <img class="activexx" src=<?= Yii::getAlias('@web/web/img/' . $img_slides[0]['image']) ?>>
    <?php for ($i = 1; $i < count($img_slides); $i++) { ?>
        <img src=<?= Yii::getAlias('@web/web/img/' . $img_slides[$i]['image']) ?>>
    <?php } ?>
</div>


<script type="text/javascript" src=<?= Yii::getAlias('@web/web/js/vendor/jquery-1.10.1.min.js') ?>></script>
<script type="text/javascript">

    function slideSwitch() {
        var $active = $('#slideshowxx IMG.activexx');

        if ($active.length == 0) $active = $('#slideshowxx IMG:last');

        // use this to pull the images in the order they appear in the markup
        var $next = $active.next().length ? $active.next()
            : $('#slideshowxx IMG:first');

// xóa ghi chú ở 3 dòng bên dưới nếu bạn muốn hiển thị ảnh ngẫu nhiên

        var $sibs = $active.siblings();
        var rndNum = Math.floor(Math.random() * $sibs.length);
        var $next = $($sibs[rndNum]);

        $active.addClass('last-activexx');

        $next.css({opacity: 0.0})
            .addClass('activexx')
            .animate({opacity: 1.0}, 1000, function () {
                $active.removeClass('activexx last-activexx');
            });
    }

    $(function () {
        setInterval("slideSwitch()", 10000);
    });

</script>
<script type="text/javascript">
    <?php if($list_img) {?>
    var list_img = Array(<?php
        foreach ($list_img as $img)
            echo '\'' . Yii::getAlias('@web/web/img/' . $img['image']) . '\',';
        ?>'null');
    list_img.pop();
    var prev = document.getElementsByClassName('prev')[0];
    var next = document.getElementsByClassName('next')[0];
    var img = document.getElementById('img');
    i = 0;
    var max = list_img.length - 1;
    prev.addEventListener('click', function () {
        i--;
        if (i == -1) i = max;
        img.src = list_img[i];
    });
    next.addEventListener('click', function () {
        i++;
        if (i == max + 1) i = 0;
        img.src = list_img[i];
    });
    <?php }?>

</script>

<style>
    #slideshowxx {
        position: relative;
        height: 350px;
        z-index: -1;
    }

    #slideshowxx IMG {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 8;
        opacity: 0.0;
    }

    #slideshowxx IMG.activexx {
        z-index: 10;
        opacity: 1.0;
    }

    #slideshowxx IMG.last-activexx {
        z-index: 9;
    }

    #slideshowxx img {
        /* Set rules to fill background */
        min-height: 100%;
        min-width: 1024px;

        /* Set up proportionate scaling */
        width: 100%;
        height: auto;

        /* Set up positioning */
        position: fixed;
        top: 0;
        left: 0;
    }

    .box_img {
        width: 100%;
        background-color: #fff;
        border-radius: 20px;
        padding: 20px 0;
    }

    .box_img > img {
        width: 100%;
        margin-bottom: 20px;
    }

    .prev {
        background-image: url(<?= Yii::getAlias('@web/web/images/Prev.png')?>);
        width: 30px;
        height: 30px;
        background-size: cover;
        float: left;
        cursor: pointer;
    }

    .next {
        background-image: url(<?= Yii::getAlias('@web/web/images/Next.png')?>);
        width: 30px;
        height: 30px;
        background-size: cover;
        cursor: pointer;
        float: right;
    }

    .profile {
        width: 100%;
        background: #fff;
        border-radius: 20px;
        margin-top: 40px;
        padding: 30px 30px 30px 50px;
    }

    .profile table {
        width: 100%;
    }

    .profile input {
        border: 0px;
        box-shadow: none;
    }

    .profile .submit {
        width: 200px;
    }

    .upload {
        width: 30px;
        height: 30px;
        margin: 0px 35px;
    }

    .profile .title {
        color: #000;
    }

    .profile table td {
        padding: 10px;
    }
</style>


