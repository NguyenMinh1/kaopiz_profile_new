<div class="title">
    <b>Images Infomation</b>
</div>
<?=\yii\helpers\Html::a('+ Add Image', \yii\helpers\Url::to(['admin-image/add']), ['class' => 'btn btn-primary'])?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'caption' => 'Kaopiz\'s Images',
    'columns' => [
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'delete' => function($model, $key, $index){
                    return \yii\helpers\Html::a('<span class="glyphicon glyphicon-trash"></span>',
                        \yii\helpers\Url::to(['admin-image/delete?id='.$key->id]),
                        ['onclick' => 'return checkDeleteImage()']);
                }
            ],
        ],
        'id',
        'member_id',
        'name',
        [
            'attribute' =>  'image',
            'label' => 'Image Name'
        ],
        [
            'content' => function($model){
                return "<img src='$model->img_url' height='50px' width='50px'>";
            }
            
        ]
    ]
]);
?>