<div class="title">
    <b>Add Image</b>
</div>
<?php
$form = \yii\widgets\ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'method' => 'post',
]);
echo $form->field($model, 'name');
echo $form->field($model, 'member_id')->textInput(['value' => Yii::$app->user->id]);
echo '<b>Image</b><br>';
echo \yii\helpers\Html::fileInput('upload_img',null,['id' => 'upload_img']);
echo '<br>';
echo \yii\helpers\Html::button('Save', ['class' => 'btn btn-primary', 'onclick' => 'checkImageSize()']);
echo ' '.\yii\helpers\Html::a('Cancle', \yii\helpers\Url::to(['admin-image/index']), ['class' => 'btn btn-warning']);
\yii\widgets\ActiveForm::end();
?>
<script>
    function checkImageSize(){
        var file = document.getElementById("upload_img").files[0];
        if(file && file.size > 1024*1024*2){
            alert("File size must small than 2 Mb");
        } else {
            document.forms[0].submit();
        }
    }
</script>
