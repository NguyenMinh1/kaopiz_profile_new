<?php
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\MyUrl;
?>
<div class="title">
    <b>Update Member Infomation</b>
</div>
<?php
$form = ActiveForm::begin();
echo $form->field($model, 'email')->textInput(['readonly' => 'readonly']);
echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'sex')->dropdownList(
    ['male'=>'Male', 'female' => 'Female']
);
//echo $form->field($model, 'birthday')->widget(DateRangePicker::classname(), [
//    //'language' => 'ru',
//    //'dateFormat' => 'dd/MM/yyyy',
//    'convertFormat'=>true,
//    'language'=>'en',
//    'pluginOptions'=>[
//        'singleDatePicker'=>true,
//        'showDropdowns'=>true,
//        'locale'=>['format'=>'Y-m-d'],
//    ]
//]);
echo $form->field($model, 'birthday')->input('date');

echo $form->field($model, 'address')->textInput();
echo $form->field($model, 'phone')->textInput();
echo $form->field($model, 'height')->textInput();
echo $form->field($model, 'weight')->textInput();
echo $form->field($model, 'country')->textInput();
echo $form->field($model, 'marital')->textarea(["rows" => 10]);
echo $form->field($model, 'hobby')->textarea(["rows" => 10]);
echo $form->field($model, 'forte')->textarea(["rows" => 10]);
echo $form->field($model, 'foible')->textarea(["rows" => 10]);
echo $form->field($model, 'nickname')->textInput();
echo $form->field($model, 'sky')->textInput();
echo $form->field($model, 'facebook')->textInput();
echo $form->field($model, 'is_banned')->checkbox();
$member_role = \app\models\Role::findOne($model->role_id);
$member_roleName = $member_role != null? $member_role->name: '';
$roles = \app\models\Role::find()->all();
$roleNames =array();
foreach ($roles as $role){
    $roleNames[$role->id] =  $role->name;
}
echo $form->field($model, 'role_id')
    ->dropDownList(
        $roleNames
    )
    ->label('Role');
echo Html::submitButton('Save', ['class' => 'btn btn-primary']);
echo " ";
echo Html::a('Cancle',Url::to(['admin-member/index']) ,['class' => 'btn btn-warning']);
ActiveForm::end();
?>

