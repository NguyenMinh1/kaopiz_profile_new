<?php 
	use yii\helpers\Html;
	use yii\helpers\Url;
 ?>
<div class="title">
	<b><?= $member->name.'\'s';?> Infomation</b>
</div>
<div>
	<div class='avatar'>
		<img src="" alt="">
	</div>
	<div>
		<p>Email: <?=$member->email ?></p>
		<p>Name:  <?=$member->name ?></p>
		<p>Sex:   <?=$member->sex ?></p>
		<p>Birthday: <?=$member->birthday ?></p>
		<p>Address: <?=$member->address ?></p>
		<p>PhoneNumber: <?=$member->phone ?></p>
		<p>Height: <?=$member->height ?></p>
		<p>Weight: <?=$member->weight ?></p>
		<p>Country: <?=$member->country ?></p>
		<p>Marital:   <?=$member->marital ?></p>
		<p>Hobby:   <?=$member->hobby ?></p>
		<p>Forte:   <?=$member->forte ?></p>
		<p>Foible:   <?=$member->foible ?></p>
		<p>NichName:   <?=$member->nickname ?></p>
		<p>Sky:   <?=$member->sky ?></p>
		<p>Facebook:   <?=$member->facebook ?></p>
		<p>Role: <?php
				$role = \app\models\Role::findOne($member->role_id);
				echo $role != null? $role->name:'';
			?></p>
	</div>
	<div>
		<?php //echo Html::a('Go Back', $goBackUrl, ['class' => 'btn btn-primary']); ?>
		<?php echo Html::button('Go Back', ['onclick' => 'history.go(-1)', 'class' => 'btn btn-primary'])?>
	</div>
</div>

