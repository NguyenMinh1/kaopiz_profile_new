<?php 
	use yii\grid\GridView;
	use app\models\Role;
	use yii\helpers\Html;
	use yii\helpers\Url;
	
?>
<div class="title">
	<b>Members Infomation</b>
</div>
<?php 
	use yii\widgets\ActiveForm;
	$title = 'abaga';
	$form = ActiveForm::begin([
			'method' => 'get',
		]);
 ?>
<?php 
	echo $this->render('_search',[
		'model' => $model,
		'form' => $form,
		'roleList' => $roleList,
	]);
 ?>
<div class='clear-float'>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'caption' => 'Kaopiz\'s Members',
    'layout' => "{summary}\n{items}\n{pager}",
    'columns' => [
    	[
            'attribute' => 'is_banned',
            'content' => function($model, $key, $index, $column){
                $result = Html::checkbox('banned[]',$model->is_banned == 1, ['value' => $model->id]);
                $result .= Html::hiddenInput('banned[]',-$model->id);
                return $result;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Action',
            'template' => '{view} {update}',
        ],
    	'id',
    	'email',
    	'name',
    	'sex',
    	'phone',
    	'country',
    	[
            'attribute' => 'role_id',
            'label' => 'Role' ,
            'content' => function($model, $key, $index, $column){
				if($model['role_id'] == null) return '(not set)';
				return Html::a(Role::getRoleName($model['role_id']), Url::to(['admin-role/view?id='.$model['role_id']]));
            }
        ],
    ],
]); ?>

<?= Html::submitButton('Apply changes',['class' => 'btn btn-primary', 'name' => 'apply-change', 'value' => 'apply-change']);?>
<?php ActiveForm::end(); ?>
</div>
