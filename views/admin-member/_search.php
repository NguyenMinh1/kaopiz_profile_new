<?php use yii\helpers\Html; ?>
<div>
	<div class='col-40 search-field'>
		<?php echo $form->field($model, 'email')->textInput()->label('Email'); ?>
	</div>
	<div class='col-40 search-field'>
		<?php echo $form->field($model, 'name')->textInput()->label('Name'); ?>
	</div>
	<div class='col-40 search-field'>
		<?php echo $form->field($model, 'country')->textInput()->label('Country'); ?>
	</div>
	<div class='col-40 search-field'>
		<?= $form->field($model, 'role_id')->label('Role')->dropdownList($roleList);?>
	</div>
	<div class='col-40 search-field'>
		<?= $form->field($model, 'sex')->radioList(['male'=>'Male', 'female' => 'Female'])->label('Sex');?>
	</div>
	<div class='col-40 search-field'>
		<?= Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'search', 'value' => 'Search'])?>
	</div>
</div>