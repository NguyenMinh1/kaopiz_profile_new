<?php 
	use yii\widgets\ActiveForm;
	use yii\grid\GridView;
	use yii\helpers\Html;
	use yii\helpers\Url;
	$form = ActiveForm::begin();
 ?>
	<div class="title">
		<b>Role Infomation</b>
	</div>
<div>
	<div>
		<p><h4><?php echo $form->field($model, 'name')->label('Role Name'); ?></h4></p>
		<p><h4><?php echo $form->field($model, 'desc')->label('Description'); ?></h4></p>
	</div>
	<div>
		<table><h4><b>Permission</b></h4>
			<tr>
				<?php foreach ($allPermissions as $per) { ?>
				<td style="padding: 5px;"><?= $per->description; ?></td>
				<?php  }?>
			</tr>
			<tr>
				<?php foreach ($allPermissions as $per) { ?>
				<td style="padding: 5px;">
					<?= Html::checkbox('check['.$per->name.'_checked]',in_array($per->name, $permissionNames), ['value' => 1])
					.Html::hiddenInput('check['.$per->name.']', -1); ?>
				</td>
				<?php  }?>
			</tr>
		</table>
	</div>
</div>
<br>
<?= Html::submitButton('Update Role', ['class' => 'btn btn-primary']); ?>
<?php
echo " ";
echo Html::a('Cancle',Url::to(['admin-role/index']) ,['class' => 'btn btn-warning']);
?>
<?php ActiveForm::end(); ?>