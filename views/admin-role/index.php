<?php 
	use yii\grid\GridView;
	use yii\helpers\Html;
	use yii\helpers\Url;
 ?>

	<div class="title">
		<b>Roles Infomation</b>
	</div>
 <?php
	echo Html::a('+ Add Role', Url::to(['admin-role/add']), ['class' => 'btn btn-primary']);
 	echo GridView::widget([
 			'dataProvider' => $dataProvider,
 			'columns' => [
 				'id',
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{delete}',
					'buttons' => [
						'delete' => function($model, $key, $index){
							$options = [
								'title' => Yii::t('yii', 'Delete'),
								'aria-label' => Yii::t('yii', 'Delete'),
								'data-confirm' => Yii::t('yii', 'Are you sure you want to remove this Role?'),
								'data-method' => 'post',
								'data-pjax' => '0',
							];
							return Html::a('<span class="glyphicon glyphicon-trash">Remove</span>',
								Url::to(['admin-role/delete?id='.$key->id]),
								['onclick' => 'return checkDeleteRole()']);
						}
					],
				],
 				[
 					'attribute' => 'name',
 					'content' => function($model, $key, $index, $column){
						return Html::a($model->name, Url::to(['admin-role/view?id='.$model->id]));
		            }
 				],
 				[
 					'attribute' => 'desc',
 					'label' => 'Description'
 				],

 			]
 		]);
  ?>